pub fn get_manifest(download_url: String) -> Result<Manifest, minreq::Error> {
    let os = from_utf8(&PH_OS[0 .. PH_OS.len() -1]);
    
    // get minfest url
    let manifest_url = [download_url, 
                        "0/".to_string(), 
                        os.unwrap().to_string(), 
                        "/Version.manifest".to_string()].join("");
    info!("manifest_url: '{}'", manifest_url);
    
    // fetch the manifest file and and store it in a Vec<u8>
    let manifest_vec = match http_get(manifest_url.to_string()) {
        Ok(buf) => buf,
        Err(e)  => return Err(e) // { println!("'{}'", e); return () },
    };
    
    // parse the manifest file into struct Manifest
    let manifest_str = from_utf8(&manifest_vec).unwrap();
    let manifest: Manifest = parse_manifest(manifest_str.to_string());
    
    Ok(manifest)
}

pub fn http_get_buffered(url: String, fd: &mut File) -> Result<(), minreq::Error> {
    let mut buffer_length = 0;
    let mut buffer = Vec::new();

    // this timeout waits for completion, not for connection?
    for byte in minreq::get(url).with_timeout(5).send_lazy()? {
        // The connection could have a problem at any point during the
        // download, so each byte needs to be unwrapped. An IO error
        // of the WouldBlock kind may also be returned, but it is not
        // a fatal error, it just means that we're still waiting for
        // more bytes. Some operating systems just block while waiting
        // for more bytes, others return a WouldBlock error.
        let (byte, _len) = match byte {
            Ok((byte, len)) => (byte, len),
            Err(minreq::Error::IoError(err)) if err.kind() == std::io::ErrorKind::WouldBlock => {
                continue
            }
            Err(err) => return Err(err),
        };

        buffer.push(byte);
        buffer_length += 1;
        
        // downloads seem to be very slow on macos if we do not 
        // buffer a couple of bytes here
        if buffer_length % (1024 * 4) == 0 {
            // write byte to file descriptor
            match fd.write(&buffer) {
                Err(err) => return Err(minreq::Error::IoError(err)),
                Ok(_)    => ()
            }

            buffer_length = 0;
            buffer.clear();
        }
    }

    if buffer_length > 0 {
        // write byte to file descriptor
        match fd.write(&buffer) {
            Err(err) => return Err(minreq::Error::IoError(err)),
            Ok(_)    => ()
        }
    }

    Ok(())
}

pub fn http_get(url: String) -> Result<Vec<u8>, minreq::Error> {
    let mut buffer = Vec::new();

    for byte in minreq::get(url).with_timeout(5).send_lazy()? {
        // The connection could have a problem at any point during the
        // download, so each byte needs to be unwrapped. An IO error
        // of the WouldBlock kind may also be returned, but it is not
        // a fatal error, it just means that we're still waiting for
        // more bytes. Some operating systems just block while waiting
        // for more bytes, others return a WouldBlock error.
        let (byte, len) = match byte {
            Ok((byte, len)) => (byte, len),
            Err(minreq::Error::IoError(err)) if err.kind() == std::io::ErrorKind::WouldBlock => {
                continue
            }
            Err(err) => return Err(err),
        };

        // The `byte` is the current u8 of data we're iterating
        // through.
        //print!("{}", byte as char);

        // The `len` is the expected amount of incoming bytes
        // including the current one: this will be the rest of the
        // body if the server provided a Content-Length header, or
        // just the size of the remaining chunk in chunked transfers.
        buffer.reserve(len);
        buffer.push(byte);

    }
    Ok(buffer)
}
