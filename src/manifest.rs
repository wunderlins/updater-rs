#[derive(Debug)]
#[allow(dead_code)]
pub struct ManifestEntry {
    pub key: String,
    pub value: String,
}

#[derive(Debug)]
pub struct Manifest {
    pub version: String,
    pub timestamp: String,
    pub version_str: String,
    pub entries: Vec<ManifestEntry>,
}

pub fn parse_manifest(buffer: String) -> Manifest {
    // println!("{}", buffer);

    let mut m: Manifest = Manifest {
        version: "".to_string(),
        timestamp: "".to_string(),
        version_str: "".to_string(),
        entries: Vec::new(),
    };

    // split string buffer by new line
    let lines = buffer.split("\n");
    for l in lines {
        // println!("{}", l);
        let parts = l.split_whitespace();
        let x: Vec<_> = parts.collect();

        if x.len() == 3 {
            m.version     = x[0].to_string();
            m.timestamp   = x[1].to_string();
            m.version_str = x[2].to_string();
        } else {
            if x.len() != 2 { continue; }

            let e: ManifestEntry = ManifestEntry {
                key:   x[0].to_string(),
                value: x[1].to_string(),
            };
            m.entries.push(e);
        }

        //print!("\n");
    }

    //println!("{:?}", m);
    return m;
}
