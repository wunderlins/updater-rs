extern crate libc;

use std::mem::transmute;
use std::fmt;
use std::ptr::null_mut;
use std::slice;

include!("../bindings.rs");

pub enum PhMagic {
    No = 0x00,
    V1,
    V2,
}

// formatter for printing values
impl fmt::Display for PhMagic {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            PhMagic::No => write!(f, "No"),
            PhMagic::V1 => write!(f, "V1"),
            PhMagic::V2 => write!(f, "V2"),
        }
    }
}

// int has_magic(unsigned char *buffer, long buflen);
pub fn c_has_magic(mut buffer: Vec<u8>) -> PhMagic {
    #[link(name="luaph1", kind="static")]
    extern { 
        fn has_magic(
            buffer: *mut ::std::os::raw::c_uchar,
            buflen: ::std::os::raw::c_long,
        ) -> ::std::os::raw::c_int;
    }

    let magic_int: i32 = unsafe {
        //ret = has_magic(buffer.into_raw() as *mut u8, 7);
        #[cfg(windows)]
        let ret: i32 = has_magic(buffer.as_mut_ptr(), (buffer.len()+1) as i32 );
        #[cfg(not(windows))]
        let ret: i32 = has_magic(buffer.as_mut_ptr(), (buffer.len()+1) as i64 );
        ret
    };
    
    // println!("Has magic: {}", magic_int);
    let magic: PhMagic = unsafe { transmute(magic_int as i8) };

    magic
}

// int decrypt_str(barr_t *in, barr_t *out);
pub fn c_decrypt_str(mut input: Vec<u8>) -> Vec<u8> {

    #[cfg(windows)]
    let mut inp: barr_t = barr_t {
        length:    (input.len() + 1) as u32,
        allocated: (input.len() + 1) as u32,
        buffer:    input.as_mut_ptr(),
    };

    #[cfg(not(windows))]
    let mut inp: barr_t = barr_t {
        length:    (input.len() + 1) as u64,
        allocated: (input.len() + 1) as u64,
        buffer:    input.as_mut_ptr(),
    };

    let mut out: barr_t = barr_t {
        length: 0,
        allocated: 0,
        buffer: null_mut(),
    };

    #[link(name="luaph1", kind="static")]
    extern {
        pub fn decrypt_str(in_: *mut barr_t, out: *mut barr_t) -> ::std::os::raw::c_int;
    }

    // use c function to unencrypt 
    let ret: i32 = unsafe {
        let ret: i32 = decrypt_str(&mut inp as *mut _, &mut out as *mut _);
        ret
    };

    // check for errors
    if ret != 0 {
        ()
    }

    // convert output buffer to vector
    let ret = unsafe {
        let v = slice::from_raw_parts(out.buffer, out.length as usize).to_vec();
        v
    };

    // FIXME: we need a C function to as an barr_t to free allocated memory

    // return new buffer
    ret
}
