#![allow(deref_nullptr)]
#![allow(deref_nullptr)]
#[macro_use]
extern crate ini;
extern crate log;
extern crate simplelog;
//extern crate failure;

use simplelog::*;
use std::path::Path;
use std::env;
use std::str::from_utf8;
use std::fs;
use std::fs::File;
use std::io::{BufReader, Read, Write};
use log::{info, warn, error, debug, trace};
use simplelog::LevelFilter;
use std::str::FromStr;

include!("./libluaph1.rs");
include!("./sha256.rs");
include!("./http.rs");

pub mod spawn;
pub mod manifest;

use spawn::spawn;
use manifest::{Manifest, parse_manifest};

#[allow(unused)]
pub fn fail(exit_code: i32, message: &str) -> &String {
    error!("{}", message);
    std::process::exit(exit_code);
    #[allow(dead_code)]
    return &message.to_string();
}

/**
 * remove last byte of a C String
 * 
 * get rid of the Terminating '\0' of the C String
 */ 
fn remove_trailing_null(vec: &[u8]) -> std::result::Result<&str, std::str::Utf8Error>{
    let slice = &vec[0 .. vec.len() -1];
    return from_utf8(slice);
}

/**
 * read a config file
 * 
 * Parameter 1 `url_config` may contan a String point to a http or file resource. The file 
 * resource may not use any protocol prefix, just the fpath to the file.
 */
pub fn read_config(url_config: String) -> Result<String, Box<dyn error::Error>> {
    // get config file content
    let mut buf: Vec<u8> = Vec::new();
    if &url_config.to_ascii_lowercase()[0..4] == "http" {
        // download file and store it in a Vec<u8> buffer
        buf = http_get(url_config.to_string())?;
    } else {
        // read file
        let mut file = File::open(url_config)?;
        file.read_to_end(&mut buf)?;
    }

    if buf.len() == 0 {
        let e: Box<dyn error::Error> = String::from("Failed to download config file, buffer length is 0").into();
        return Err(e);
    }
    info!("Downloaded config file");

    // decrypt config file
    let res;
    let has_magic: PhMagic = c_has_magic(buf.to_owned());
    if matches!(has_magic, PhMagic::V2) {
        res = c_decrypt_str(buf);
    } else {
        res = buf;
    }

    // vector to string
    let ini_content = from_utf8(&res)?;
    info!("Parsed config file");
    Ok(ini_content.to_string())
}

/**
 * Protohand updater
 * 
 * This program should be installed in a subfolder of protohand 
 * and will be run when there needs to be a new version installed.
 * 
 * The following required programm arguments are expected (in this order)
 * 
 * 1. path or http url to the current ph config file
 * 2. current running ph.exe version (this must be provided by ph.exe itself)
 * 3. the url that was called, after update ph.exe will be called with this url.
 */
fn main() {
    // env_logger::init();

    let log_level: LevelFilter = match env::var("RUST_LOG") {
        Ok(ll) => LevelFilter::from_str(&ll).unwrap(),
        Err(_) => LevelFilter::Info,
    };

    CombinedLogger::init(
        vec![
            TermLogger::new(log_level, Config::default(), TerminalMode::Mixed, ColorChoice::Auto),
            WriteLogger::new(log_level, Config::default(), File::create("updater.log").unwrap()),
        ]
    ).unwrap();

    // get rid of the Terminating '\0' of the C String
    let os = remove_trailing_null(PH_OS).unwrap();
    info!("os: {}", os);

    let args: Vec<String> = env::args().collect();
    info!("args: {:?}", args);

    let url_config =  match args.get(1) { // "http://localhost:8081/example.d2t";
        Some(url) => url,
        None => fail(1, "Expected parameter 1 to be config url or File.")
    };
    info!("url_config: {}", url_config);

    let current_version =  match args.get(2) { 
        Some(str) => str,
        None => fail(2, "Expected parameter 2 to contain Version to compare to."),
    };
    info!("current_version: {}", current_version);
    
    let url_to_call =  match args.get(3) { 
        Some(str) => str,
        None => fail(3, "Expected parameter 3 to contain the URL that was called by ph.exe."),
    };
    info!("url_to_call: {}", url_to_call);
    
    // get config file content
    let ini_content: String = match read_config(url_config.to_string()) {
        Ok(c)  => c,
        Err(e) => {
            fail(4, &format!("Failed to open config file: {}", e));
            return ()
        }
    };
    trace!("{}", ini_content);

    let ini = inistr!(&ini_content);
    let mut download_url: String = "".to_string();
    if ini["_global"].contains_key("download_url") == true {
        download_url = match &ini["_global"]["download_url"] {
            Some(url) => url.to_string(),
            None  => {
                fail(5, &format!("Download url not configured."));
                return ();
            }
        };
    } else {
        fail(0, &format!("Download url not configured."));
    }
    info!("download_url: {}", download_url);

    let download_version = ini["_global"]["version"].clone().unwrap();
    if current_version.eq_ignore_ascii_case(&download_version) {
        // we are already on the latest version, abort
        warn!("already at latest version. requested: {}, got: {}. exit.", download_version, current_version);
        return ();
    }
    info!("download_version: {}", download_version);

    // get manifest of current release    
    let manifest: Manifest = match get_manifest(download_url.clone()) {
        Ok(m)  => m,
        Err(e) => { 
            fail(6, &format!("Failed to fetch Manifest: {}", e));
            return ();
        }
    };
    trace!("{:#?}", manifest);

    // sanity check if manifest is valid
    if manifest.version == "" { fail(7, &format!("Error in Version.manifest, no version defined")); }
    if manifest.version_str == "" { fail(7, &format!("Error in Version.manifest, no version_str defined")); }
    if manifest.timestamp == "" { fail(7, &format!("Error in Version.manifest, no timestamp defined")); }

    info!("manifest.version: {}", manifest.version);
    info!("manifest.version_str: {}", manifest.version_str);
    info!("manifest.timestamp: {}", manifest.timestamp);

    // prepare a temp location where we can download all the files to
    let temp = match env::var("TEMP") {
        Ok(temp) => temp,
        Err(_) => "/tmp".to_string(),
    };
    let temp_folder = Path::new(&temp).join("ph_updater");
    info!("temp_folder: {}", temp_folder.display().to_string());

    // remove temp folder, make sure we get rid of old files first
    match std::fs::remove_dir_all(temp_folder.to_owned()) {
        Ok(_)  => (),
        Err(_) => (),
    }
    
    // make sure the temp base folder exists
    match fs::create_dir_all(&temp_folder) {
        Ok(_) => (),
        Err(error) => {
            fail(8, &format!("Failed to create Temp Folder: {}", error));
            return ();
        }
    }

    // loop over all files
    for f in &manifest.entries {
        let dst_path = temp_folder.to_owned();
        let dst_path = dst_path.join(&f.value);

        let src_url = [download_url.clone(), 
            "0/".to_string(), os.to_string(), 
            "/".to_string(), f.value.to_string()].join("");

        debug!("{}: {}", dst_path.display().to_string(), src_url);

        // check if folder to path exists
        let mut folder = dst_path.clone();
        folder.pop();
        if ! folder.exists() {
            match fs::create_dir_all(&folder) {
                Ok(_) => (),
                Err(error) => {
                    fail(9, &format!("Failed to create Folder: {}", error));
                    return ();
                }
            }
        }

        // create file handle
        let mut file = match File::create(dst_path.to_owned()) {
            Ok(f) => f,
            Err(err) => {
                fail(10, &format!("Failed to create file {}: {}", dst_path.display().to_string(), err));
                return ();
            }
        };

        // download file
        match http_get_buffered(src_url.to_owned(), &mut file) {
            Ok(r)  => r,
            Err(e) => {
                fail(11, &format!("Failed to download {}: {}", src_url, e));
                return {};
            }
        }
        drop(file); // take file out of scpe, close it

        // check sha256
        let hash: String = match sha256_hash(dst_path.to_owned().display().to_string()) {
            Ok(d) => d,
            Err(e) => {
                fail(12, &format!("Error while hashing: {}", e));
                return ();
            }
        };

        if ! hash.eq_ignore_ascii_case(&f.key) {
            fail(13, &format!("Hash of the following file did not match: {}", dst_path.display().to_string()));
        }
    }
    info!("Downloaded all files from manifest");

    #[cfg(windows)]
    let exe = "./ph.exe";
    #[cfg(not(windows))]
    //let exe = "./ph";
    let exe = "../protohand-lua/build/ph";
    let exe_bak = [exe.to_string(), ".old".to_string()].join("");

    // rename original binary before moving files to make sure there 
    // are no locks on the executable

    if Path::new(exe).exists() {
        // remove old backup if there is any
        if Path::new(&exe_bak).exists() {
            match fs::remove_file(&exe_bak) {
                Ok(_) => (),
                Err(e) => {
                    fail(14, &format!("Failed to delete old exe backup exe: {}", e));
                    return ();
                }
            }
        }

        /*
        match fs::rename(exe, exe_bak) {
            Ok(_) => (),
            Err(e) => {
                fail(15, &format!("Failed renaming exe: {}", e));
                return ();
            }
        }
        */
    }

    // TODO: copy files to final location
    for f in &manifest.entries {
        let src_path = temp_folder.to_owned().join(&f.value);
        let dst_path = Path::new("./").join(&f.value);

        // make sure the destination folder exists
        let mut dst_folder = dst_path.clone();
        dst_folder.pop();
        match fs::create_dir_all(dst_folder) {
            Ok(_) => (),
            Err(error) => {
                fail(16, &format!("Failed to create Destination Folder: {}", error));
                return ();
            }
        }

        debug!("Copy {} to {}", &src_path.display().to_string(), 
                                &dst_path.display().to_string());
        //;
    }
    info!("Copied all downloaded files into place");

    // now that we have successfully finished, execute original url
    let args = vec![url_to_call.to_string(), url_config.to_string()];
    //let args = vec!["30".to_string()];

    // FIXME: figure out final location of before calling
    //let exe  = "/usr/bin/sleep";
    info!("calling: {} {:?}", exe, args);
    spawn(exe.to_string(), args);

    ()
}

#[cfg(test)]
mod updater_tests {
    use super::*;
    use std::path::PathBuf;
    //use std::env;

    fn get_root_dir() -> PathBuf {
        let mut root_dir = std::env::current_exe().unwrap();
        root_dir.pop(); // remove file name
        let root_dir = root_dir; // make it r/o again
        root_dir
    }

    /*
    #[test]
    fn main_test() {
        let mut args: Vec<String> = env::args().collect();
        // remove the test arguments
        args.remove(1);
        args.remove(1);

        // output and test
        println!("{:?}", args);
        assert_eq!(4, args.len());
    }
    */

    #[test]
    fn download_config() {
        let download_url = "http://localhost:8081/example.d2t".to_string();
        let config = read_config(download_url);
        assert!(!config.is_err());
    }

    #[test]
    fn remove_trailing_null_test() {
        let s  = b"Linux\0";
        let s1 = remove_trailing_null(s);
        assert_eq!("Linux", s1.unwrap());
    }

    #[test]
    fn read_config_test() {
        //pub fn read_config(url_config: String) -> Result<String, Box<dyn error::Error>> {
        let root_dir = get_root_dir();

        let mut exe_dir = root_dir.clone();
        exe_dir.pop(); exe_dir.pop();exe_dir.pop(); // cd ../../..
        let ini_encryptedv1 = exe_dir.clone().join("testdata").join("example.dat");
        let ini_encryptedv2 = exe_dir.clone().join("testdata").join("example.d2t");
        let ini_unencrypted = exe_dir.clone().join("testdata").join("example.ini");

        // println!("{:#?}\n{:#?}\n{:#?}\n", ini_encryptedv1, ini_encryptedv2, ini_unencrypted);
        let data_encryptedv1 = read_config(ini_encryptedv1.display().to_string()).unwrap();
        let data_encryptedv2 = read_config(ini_encryptedv2.display().to_string()).unwrap();
        let data_unencrypted = read_config(ini_unencrypted.display().to_string()).unwrap();

        assert_eq!(data_encryptedv2, data_unencrypted);
        assert_ne!(data_encryptedv1, data_unencrypted);

        // test with http download
        let data_encryptedv2_http =  read_config(String::from("http://localhost:8081/example.d2t"));
        assert_eq!(data_encryptedv2, data_encryptedv2_http.unwrap());
    }
    
}
