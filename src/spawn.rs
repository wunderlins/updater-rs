use std::process::Command;
extern crate log;
use log::{debug};

pub fn spawn(cmd: String, args: Vec<String>) {
    /*
    if let Ok(Fork::Child) = daemon(false, false) {
        Command::new(cmd)
            .args(args)
            .output()
            .expect("failed to execute process");
    }
    */
    /*
    let ret = Exec::cmd(cmd)
        .args(&args)
        .detached()
        .capture();
    */
    let ret = Command::new(cmd)
        .args(args)
        .output()
        .expect("failed to execute process");

    debug!("{:?}", ret);
}