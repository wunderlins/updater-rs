use data_encoding::HEXUPPER;
use ring::digest::{Context, Digest, SHA256};
use std::error;

fn sha256_digest<R: Read>(mut reader: R) -> Result<Digest, std::io::Error> {
    let mut context = Context::new(&SHA256);
    let mut buffer = [0; 1024];

    loop {
        let count = reader.read(&mut buffer)?;
        if count == 0 {
            break;
        }
        context.update(&buffer[..count]);
    }

    Ok(context.finish())
}

fn sha256_hash(file_path: String) -> Result<String, Box<dyn error::Error>> {
    // check sha256
    let input = File::open(file_path.to_owned())?;

    let reader = BufReader::new(input);
    let digest = sha256_digest(reader)?;

    return Ok(HEXUPPER.encode(digest.as_ref()));
}