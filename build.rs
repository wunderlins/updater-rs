#[cfg(windows)]
extern crate winres;

extern crate bindgen;
extern crate cc;
extern crate pkg_config;

use std::env;
use std::path::Path;
//use std::os::unix::io::{FromRawFd, IntoRawFd};
//use std::path::PathBuf;

fn main() {

    let mut wrapper_h_str = String::from("");

    // use clang on OSX to generate our Header file
    #[cfg(target_os = "macos")]
    if 1 == 1 {
        wrapper_h_str = Path::new("./libluaph1/libluaph1.h").display().to_string();
    }

    /*
     * The builtin clang preprocessor seems to mess up under linux and 
     * mingw. Therefore we just 
     * 
     * 1. invoke `cpp` 
     * 2. preprocess `libluaph1/libluaph1.h`
     * 3. store the result in the build folder as `wrapper.h`
     * 4. further down, passing the pasth to wrapper.h trough `wrapper_h_str` to the compiler.
     */
    #[cfg(not(target_os = "macos"))]
    if 1 == 1 {

        use std::process::{Command, Stdio};
        use std::fs::File;

        let out_dir = env::var("OUT_DIR").unwrap();

        // create wrapper.h with cpp (the C pre processor). clang seems to fail 
        // to resolve some headers for whatever reasons on linux

        // let out_dir = env::var("CARGO_MANIFEST_DIR").unwrap();
        let wrapper_h = Path::new(&out_dir).join("wrapper.h");
        let wrapper_fd = File::create(wrapper_h.to_owned()).unwrap();
        //println!("cargo:warning={}", wrapper_h_str);

        let mut cmd = Command::new("cpp")
            .args(["-DPH1_STANDALONE", "libluaph1/libluaph1.h"])
            .stdin(Stdio::null())
            .stdout(std::process::Stdio::from(wrapper_fd))
            .spawn()
            .expect("ls command failed to start");
        cmd.wait().unwrap();
        wrapper_h_str = wrapper_h.to_owned().as_path().display().to_string();
    }
    

    // Tell cargo to invalidate the built crate whenever the wrapper changes
    println!("cargo:rerun-if-changed=libluaph1/libluaph1.h");

    pkg_config::Config::new()
        .atleast_version("1.2")
        .probe("zlib")
        .unwrap();

    let bindings = bindgen::Builder::default()
        .header(wrapper_h_str)
        .clang_arg("-I/usr/include")
        .clang_arg("-DPH1_STANDALONE")

        .allowlist_var("PH_OS")

        .allowlist_type("barr_t")

        .allowlist_function("garbage_data")
        .allowlist_function("has_magic")
        .allowlist_function("xor")
        .allowlist_function("decrypt_str")
        .allowlist_function("crypt_file")
        .allowlist_function("is_encoded")
        .allowlist_function("_xor")
        .allowlist_function("transcode_str")

        .generate()
        .expect("Unable to generate bindings");

    //let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    //println!("{:#?}", "bindings.rs");
    bindings
        .write_to_file("bindings.rs")
        .expect("Couldn't write bindings!");

    cc::Build::new()
        .file("libluaph1/libluaph1.c")
        .flag("-Wall")
        .flag("-DPH1_STANDALONE")
        .compile("libluaph1.a");

    println!("cargo:rustc-link-search=all={}", env::var("OUT_DIR").unwrap());
    println!("cargo:rustc-link-lib=static=luaph1");
    println!("cargo:rustc-link-lib=static=z");
    println!("cargo:rerun-if-changed=libluaph1/libluaph1.c");
    //println!("cargo:rustc-flags=-l luaph1 -L{}", env::var("OUT_DIR").unwrap());

    // windows only
    //println!("cargo:rustc-link-args=\"../protohand-lua/ico/app.res\"");
    #[cfg(windows)]
    if cfg!(target_os = "windows") {
        let mut res = winres::WindowsResource::new();
        res.set_icon("./ico/app.ico"); // Replace this with the filename of your .ico file.
        res.compile().unwrap();
    }
    
}