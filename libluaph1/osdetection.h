#ifndef __WRAPPER_OSDETECTION_H_

#ifdef _WIN32
#ifndef WIN32_LEAN_AND_MEAN
#	define WIN32_LEAN_AND_MEAN
#endif
#	define NOGDI 1
#	define OPERATING_SYSTEM "Windows"
#endif

#ifdef __APPLE__
#	define OPERATING_SYSTEM "OSX"
#endif

#ifdef __linux__
#	define OPERATING_SYSTEM "Linux"
#endif

#ifndef OPERATING_SYSTEM
#	define OPERATING_SYSTEM "Unknown"
#endif

#define indef __WRAPPER_OSDETECTION_H_
#endif

