/**
 * returns an absolute path from a relative file name
 */

#ifndef PH1_STANDALONE
#ifdef __cplusplus
  #include "lua.hpp"
#else
  #include "lua.h"
  #include "lualib.h"
  #include "lauxlib.h"
#endif
#endif

#include <math.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "libluaph1.h"
#include "osdetection.h"
#include "libluaph1_key.h"

//#define DEBUG

#ifdef __cplusplus
extern "C"{
#endif

char encodek[20];
const char xor_magic[MAGIC_OFFSET] = MAGIC;

char* set_encodek() {
	// key for encoding the config file
	encodek[0] = '3';
	encodek[1] = 'f';
	encodek[2] = 'J';
	encodek[3] = '%';
	encodek[4] = 'd';
	encodek[5] = '6';
	encodek[6] = '5';
	encodek[7] = ',';
	encodek[8] = '$';
	encodek[9] = '/';
	encodek[10] = '@';
	encodek[11] = 'd';
	encodek[12] = 'f';
	encodek[13] = 'h';
	encodek[14] = 's';
	encodek[15] = '(';
	encodek[16] = '3';
	encodek[17] = 'l';
	encodek[19] = 0;
	
	return encodek;
}

#ifdef DEBUG
static int debug_barr(char *name, barr_t *b) {
  printf("%-10s length: %d, allocated: %d: ", name, b->length, b->allocated);
  for (int i=0; i< b->length; i++) 
    printf("%02X ", b->buffer[i]);
  printf("\n\n");
}
#endif

unsigned char *garbage_data(int len, const char key[], int key_length) {
  // generate random pos
  time_t t = time(NULL);
  unsigned int seed = (unsigned int) ((key[0] << 8) + key[0]) * t;
  srand(seed);
  unsigned long r = rand();
#ifdef DEBUG
  printf("time: %ld, key: %x %x, seed: %d, rand: %d\n", t, key[0], key[1], seed, r);
#endif
  unsigned long offset = r*100 / INT_MAX;
  unsigned int start_pos = key_length * offset / 100;

  // prevent overruns
  if (start_pos+len > (uint) key_length) {
    int offset = (start_pos+len-key_length) * 5;
    start_pos -= offset;
  }

#ifdef DEBUG
  printf("INT_MAX %d, key_length: %d, offset: %d, start_pos: %d\n", INT_MAX, key_length, offset, start_pos);
#endif

  // create unsigned char buffer
  unsigned char *garbage = malloc(len);
  memcpy(garbage, key+start_pos, len);

  return garbage;
}

/**
 * check if the unsigned char array starts with the magick number
 * 
 * returns 
 * - 0 when not found
 * - 1 for v1 magic namber
 * - 2 for v2 magic number
 */
int has_magic(unsigned char *buffer, long buflen) {
  if (buflen < MAGIC_OFFSET) return 0;
  unsigned char start[MAGIC_OFFSET+1] = {0};
  unsigned char magic1[MAGIC_OFFSET+1] = MAGIC;
  unsigned char magic2[MAGIC_OFFSET+1] = MAGIC2;
  memcpy(start, buffer, MAGIC_OFFSET);

  if (strcmp((char*) start, (char*) magic2) == 0)
    return 2;
	
  if (strcmp((char*) start, (char*) magic1) == 0)
    return 1;
	
  return 0;
}

unsigned char* xor(unsigned char *string, const char *key, long l) {
	int i = 0;
	unsigned char* out = malloc(l);
	size_t length = key_length; // strlen(key);
	for(i=0; i<l; i++) {
		out[i] = (char) (string[i] ^ key[i % length]);
	}
	return out;
}

/**
 * crypt file
 * 
 * return codes:
 * - 0: ok
 * - 1: empty input buffer
 * - 2: failed to allocate memory
 * - 3: output buffer too small
 * - 
 */
int crypt_str(barr_t *in, barr_t *out) {
  int ret = ret;

  // get rid of trailing \0
  if (in->length) {
#ifdef DEBUG
    printf("last unsigned char: %x\n", in->buffer[in->length-1]);
#endif
    if (in->buffer[in->length-1] == '\0')
      in->length = (in->length) - 1;
  } else {
    return 1;
  }

#ifdef DEBUG
  printf("crypt_str input buf len: %d\n", in->length);
#endif

  int outl = in->length;
  if (outl < 30) outl += 30; // need to allocate space for zip header
  barr_t *zip = malloc(sizeof(barr_t));
  if (zip == NULL) return 2;
  unsigned char *zip_buffer = malloc(outl);
  if (zip_buffer == NULL) return 2;
  zip->length = outl;
  zip->allocated = outl;
  zip->buffer = zip_buffer;

  // zip content
  ret = compress(zip->buffer, &(zip->length), in->buffer, in->length);
  if (ret != Z_OK) {
		if (ret == Z_MEM_ERROR) ret = 2;
		if (ret == Z_BUF_ERROR) ret = 3;

    // free zip
    free(zip_buffer);
    free(zip);
    return ret;
  }

#ifdef DEBUG
  debug_barr("in", in);
  debug_barr("zip", zip);
#endif

  // remove zip magic
  zip->buffer += 2;
  zip->length -= 2;

#ifdef DEBUG
  debug_barr("zip", zip);
#endif

  // add magic
  out->length = zip->length + MAGIC_OFFSET;
  out->allocated = out->length;
  out->buffer = malloc(out->length);
  if (zip == NULL) return 2;
  unsigned char magic[MAGIC_OFFSET+1] = MAGIC2;
  memcpy(out->buffer, magic, MAGIC_OFFSET);

  // xor content
  unsigned char *xor_data = xor(zip->buffer, key, zip->length);
  memcpy(out->buffer+MAGIC_OFFSET, xor_data, zip->length);
  free(xor_data);

#ifdef DEBUG
  debug_barr("out", out);
#endif

  // free temp data
  free(zip_buffer);
  free(zip);
  
  return ret;
}

/**
 * decrypt file
 * 
 * return codes:
 * - 0: ok
 * - 1: empty input buffer
 * - 2: failed to allocate memory
 * - 3: output buffer too small
 * - 4: corrupt input data
 */
int decrypt_str(barr_t *in, barr_t *out) {
  int ret = 0;

#ifdef DEBUG
  debug_barr("in", in);
#endif

  if (!in->length)
    return 1;

#ifdef DEBUG
  printf("decrypt_str input buf len: %d\n", in->length);
#endif

  // remove magic
  unsigned char *in_no_magic = in->buffer + MAGIC_OFFSET;
  unsigned long in_length = in->length - MAGIC_OFFSET;

  // xor content
  unsigned char *xor_data = xor(in_no_magic, key, in_length);

  // add zip magic
  barr_t *zip = malloc(sizeof(barr_t));
  if (zip == NULL) return 2;
  unsigned char *zip_buffer = malloc(in_length+2);
  if (zip_buffer == NULL) return 2;
  zip->length = in_length+2;
  zip->allocated = in_length+2;
  zip_buffer[0] = 0x78;
  zip_buffer[1] = 0x9C;
  memcpy(zip_buffer+2, xor_data, in_length);
  free(xor_data);
  zip->buffer = zip_buffer;

#ifdef DEBUG
  debug_barr("zip", zip);
#endif

  // deflate
  unsigned long alloc_out = in->length * 10; // try to be generous
  unsigned char *out_buffer = malloc(alloc_out);
  out->length    = alloc_out;
  out->allocated = alloc_out;
  out->buffer    = out_buffer;

#ifdef DEBUG
  debug_barr("out", out);
#endif

  ret = uncompress(out->buffer, &out->length, zip->buffer, zip->length);
  if (ret != Z_OK) {
		if (ret == Z_MEM_ERROR) ret = 2;
		if (ret == Z_BUF_ERROR) ret = 3;
		if (ret == Z_DATA_ERROR) ret = 4;

    // free zip
    free(zip_buffer);
    free(zip);
    return ret;
  }

#ifdef DEBUG
  debug_barr("out", out);
#endif

  free(zip_buffer);
  free(zip);
  return ret;
}

/**
 * crypt file
 * 
 * return codes
 * - 0: success
 * - 1: failed to open input file
 * - 2: failed to read input file
 * - 3: failed allocate memory
 * - 4: empty crypt input buffer
 * - 5: crypt/decrypt faield to allcoate memory
 * - 6: crypt/decrypt output buffer too small
 * - 7: decrypt corrupt input data
 * - 8: failed to open output file
 * - 9: failed to write output file
 * - 10: wrong magic number, v1 detected
 */
int crypt_file(char *file_in, char* file_out) {
  int ret = 0;
  FILE *f;
  int length = 0;

  unsigned char *string = NULL;
  unsigned char *out_data = NULL;
  barr_t *in = NULL;
  barr_t *out  = NULL;

  // read in file into buffer
	f = fopen(file_in, "rb");
	if (f == NULL) {
		return 1;
	}
	
	fseek(f, 0, SEEK_END);
	long fsize = ftell(f);
	fseek(f, 0, SEEK_SET);  //same as rewind(f);
	string = malloc(fsize);
  if (string == NULL) return 3;
	length = fread(string, 1, fsize, f);
#ifdef DEBUG
  printf("fsizes: %d, length: %d\n", fsize, length);
#endif
  if (length != fsize)
    return 2;
	fclose(f);
#ifdef DEBUG
  printf("in file length: %d\n", fsize);
#endif
  
  // check if we need to crypt or decrypt
  int magic = has_magic(string, fsize);
#ifdef DEBUG
  printf("has magic: %d\n", magic);
#endif

  // create buffers
  in = malloc(sizeof(barr_t));
  if (in == NULL) return 3;
  in->length = fsize;
  in->allocated = fsize;
  in->buffer = string;

  out = malloc(sizeof(barr_t));
  if (out == NULL) return 3;
  out->length = 0;
  out->allocated = 0;
  out->buffer = out_data;

  // magick!
  if (magic == 0) {
    ret = crypt_str(in, out);
  } else if (magic == 1) {
		ret = 10;
		goto CLEANUP;
  } else {
    ret = decrypt_str(in, out);
  }

  if (ret != 0)
    return ret + 4;
  
  // write out file
	f = fopen(file_out, "wb");
	if (f == NULL) {
		ret = 8;
    goto CLEANUP;
	}

  length = fwrite(out->buffer, 1, out->length, f);
  if (length != (int) out->length) {
    ret = 9;
  }

  CLEANUP:
  fclose(f);
  if (in->buffer != NULL)
    free(in->buffer);
  free(in);
  if (out->buffer != NULL)
    free(out->buffer);
  free(out);

  return ret;
}

int is_encoded(char* string) {
	if (string[0] == xor_magic[0] && 
		string[1] == xor_magic[1] && 
		string[2] == xor_magic[2] && 
		string[3] == xor_magic[3])
		return 1;
	return 0;
}

char* _xor(char *string, const char *key, long l) {
	int i = 0;
	char* out = malloc(l);
	size_t length = strlen(key);
	for(i=0; i<l; i++) {
		out[i] = (char) (string[i] ^ key[i % length]);
	}
	return out;
}

int transcode_str(char* strin, long* fsize, char** strout, char* key) {
	
	xormode_t mode = ENCODE;
	long outlen;
	
	if (strin[0] == xor_magic[0] && 
	    strin[1] == xor_magic[1] && 
			strin[2] == xor_magic[2] && 
			strin[3] == xor_magic[3]) {
		strin += 4;
		outlen = *fsize-4;
		mode = DECODE;
		//printf("Decode\n");
	} else {
		outlen = *fsize+4;
		//printf("Encode\n");
	}
	
	char *result = (char*) malloc(outlen);
	if (mode == ENCODE) {
		memcpy(result, xor_magic, 4);
		//printf("Before encoding: %ld\n", *fsize);
		char* encoded = _xor(strin, key, *fsize);
		//printf("After encoding:  %ld\n", *fsize);
		memcpy(result+4, encoded, *fsize);
	} else {
		memcpy(result, _xor(strin, key, *fsize), outlen);
		result[outlen] = '\0';
	}
	
	*strout = result;
	*fsize = outlen;
	
	return 0;
}

#ifndef PH1_STANDALONE

static int os_setenv(lua_State* L) {
	const char* name = luaL_checkstring(L, 1);
	const char* value = luaL_checkstring(L, 2);
	
	#ifdef _WIN32
		int len = strlen(name) + strlen(value) + 2; //adding one char for '=' and another for '\0'
		char buffer[len];
		strcpy(buffer, name);
		strcat(buffer, "=");
		strcat(buffer, value);
		buffer[len] = 0;
		int res = _putenv(buffer);
	#else
		int res = setenv(name, value, 1);
	#endif
	
	//printf("env (%d): %s\n", res, getenv(name));
	if (res != 0) {
		lua_pushnil(L);
		lua_pushfstring(L, "unable to set environment variable '%s', errno %d : %s", name, errno, strerror(errno));
		return 2;
	}
	
	lua_pushnumber(L, res);
	return 1;
}

static int os_getenv(lua_State* L) {
	const char* name = luaL_checkstring(L, 1);
	
	char *buffer;
	//printf("name: %s\n", name);
	buffer = getenv(name);
	//printf("env: %s\n", buffer);
	if (buffer == NULL) {
		lua_pushnil(L);
		lua_pushfstring(L, "unable to get environment variable '%s'", name);
		return 2;
	}
	
	lua_pushstring(L, buffer);
	return 1;
}

static int os_realpath(lua_State* L) {
	char result[PH_PATH_MAX];
	int ok;

	const char* path = luaL_checkstring(L, 1);

#ifdef __MINGW32__
	ok = (_fullpath(result, path, PH_PATH_MAX) != NULL);
	// printf("path: %s\nresult: %s\n", path, result);
#else
	ok = (realpath(path, result) != NULL);
#endif

	if (!ok) {
		lua_pushnil(L);
		lua_pushfstring(L, "unable to fetch real path of '%s', errno %d : %s", path, errno, strerror(errno));
		return 2;
	}
	
	/*
	// replace all backslashes with forward slashes
	size_t l = strlen(result); size_t i;
	for(i=0; i<l; i++) if (result[i] == '\\') result[i] = '/';
	*/
	
	lua_pushstring(L, result);
	return 1;
}

/**
 * encode a file
 *
 * Int ret = encode_file(String infile_name[, String outfile_name])
 *
 * if parameter 2 is nil, the outfile will be stored in the same folder as in 
 * file with the ending ".dat"
 */
static int lua_encode_file(lua_State* L) {
	//printf("args: %d\n", lua_gettop(L));
	const char* infile = luaL_checkstring(L, 1);
	int l = strlen(infile);
	char* outfile = (char*) malloc(sizeof(char*) * (l + 1));
	
	if (lua_gettop(L) == 2) {
		strcpy(outfile, luaL_checkstring(L, 2));
	} else {
		strcpy(outfile, infile);
		outfile[l-4] = '.';
		outfile[l-3] = 'd';
		outfile[l-2] = 'a';
		outfile[l-1] = 't';
	}
	
	FILE *f = fopen(infile, "rb");
	if (f == NULL) {
		lua_pushnumber(L, 2);
		return 2;
	}
	
	fseek(f, 0, SEEK_END);
	long fsize = ftell(f);
	fseek(f, 0, SEEK_SET);  //same as rewind(f);
	//printf("%ld\n", fsize);
	char *string = (char*) malloc(fsize);
	fread(string, fsize, 1, f);
	//string[fsize] = 0;
	fclose(f);
	
	char *result;
	//printf("fsize before: %ld\n", fsize);
	set_encodek();
	transcode_str(string, &fsize, &result, encodek);
	//printf("fsize after: %ld\n", fsize);
	
	f = fopen(outfile, "wb");
	fwrite(result, 1, fsize, f);
	if (f == NULL) {
		lua_pushnumber(L, 3);
		return 3;
	}
	
	fclose(f);
	free(outfile);
	
	lua_pushnumber(L, 0);
	return 1;
}

/**
 * read ini file, unencode if nessecary
 *
 * String contents, Int length = read_ini(String file_name)
 */
static int lua_read_ini(lua_State* L) {
	const char* file_name = luaL_checkstring(L, 1);
	char* out;

	// read file from argv[1]
	FILE *f = fopen(file_name, "rb");
	if (f == NULL)
		return 1;
	
	fseek(f, 0, SEEK_END);
	long fsize = ftell(f);
	fseek(f, 0, SEEK_SET);  //same as rewind(f);
	//printf("%ld\n", fsize);
	char *string = (char*) malloc(fsize + 1);
	fread(string, fsize, 1, f);
	string[fsize] = 0;
	fclose(f);
	
	int encoded = is_encoded(string);
	if (encoded) {
		out = (char*) malloc(fsize + 1);
		transcode_str(string, &fsize, &out, set_encodek());
	} else {
		out = string;
	}
	
	//printf(out);
	//printf("size: %d\n", fsize);
	lua_pushnumber(L, fsize);
	lua_pushlstring(L, out, fsize);
	
	free(string);
	
	// WTF: return 2 ?
	return 1;
}

/**
 * read ini string, unencode if nessecary
 *
 * String contents = read_ini(String file_name)
 */
static int lua_read_str(lua_State* L) {
	char* string = (char*) luaL_checkstring(L, 1);
	long fsize = luaL_checkinteger(L, 2);
	char* out;
	
	int encoded = is_encoded(string);
	if (encoded) {
		out = (char*) malloc(fsize + 1);
		transcode_str(string, &fsize, &out, set_encodek());
	} else {
		out = string;
	}
	
	//printf(out);
	//printf("size: %d\n", fsize);
	lua_pushnumber(L, fsize);
	lua_pushlstring(L, out, fsize);
	
	//free(string);
	
	// WTF: return 2 ?
	return 1;
}

/**
 * encode a file
 *
 * Int ret = encode_file2(String infile_name[, String outfile_name])
 *
 * if parameter 2 is nil, the outfile will be stored in the same folder as in 
 * file with the ending ".dat"
 */
static int lua_encode_file2(lua_State* L) {
	//printf("args: %d\n", lua_gettop(L));
	const char* infile = luaL_checkstring(L, 1);
	int l = strlen(infile);
	char* outfile = (char*) malloc(sizeof(char*) * (l + 1));
	
	if (lua_gettop(L) == 2) {
		strcpy(outfile, luaL_checkstring(L, 2));
	} else {
		strcpy(outfile, infile);
		outfile[l-4] = '.';
		outfile[l-3] = 'd';
		outfile[l-2] = 'a';
		outfile[l-1] = 't';
	}
	
	int ret = crypt_file((char*) infile, outfile);

	if (ret != 0) {
		char errmsg[50] = {0};
		switch (ret) {
			case 1:
				strcpy(errmsg, "failed to open input file");
				break;
			case 2:
				strcpy(errmsg, "failed to read input file");
				break;
			case 3:
				strcpy(errmsg, "failed allocate memory");
				break;
			case 4:
				strcpy(errmsg, "empty crypt input buffer");
				break;
			case 5:
				strcpy(errmsg, "crypt/decrypt faield to allcoate memory");
				break;
			case 6:
				strcpy(errmsg, "crypt/decrypt output buffer too small");
				break;
			case 7:
				strcpy(errmsg, "decrypt corrupt input data");
				break;
			case 8:
				strcpy(errmsg, "failed to open output file");
				break;
			case 9:
				strcpy(errmsg, "failed to write output file");
				break;
			case 10:
				strcpy(errmsg, "wrong magic number, v1 detected");
				break;
			default:
				strcpy(errmsg, "unhandled error");
		}

		lua_pushnumber(L, ret);
		lua_pushfstring(L, "Failed to allocate memory");
		return 2;
	}

	lua_pushnumber(L, 0);
	return 1;
}

/**
 * read ini file, unencode if nessecary
 *
 * String contents, Int length = read_ini2(String file_name)
 * 
 * if length is nil, then contents contains an error message.
 */
static int lua_read_ini2(lua_State* L) {
	const char* file_name = luaL_checkstring(L, 1);
	char* out;

	// read file from argv[1]
	FILE *f = fopen(file_name, "rb");
	if (f == NULL)
		return 1;
	
	fseek(f, 0, SEEK_END);
	long fsize = ftell(f);
	fseek(f, 0, SEEK_SET);  //same as rewind(f);
	//printf("%ld\n", fsize);
	char *string = (char*) malloc(fsize + 1);
	fread(string, fsize, 1, f);
	string[fsize] = 0;
	fclose(f);
	
	int encoded = has_magic(string, fsize);

	// old file encryption? down grade.
	if (encoded == 1) {
		char *out;
		out = (char*) malloc(fsize + 1);
		transcode_str(string, &fsize, &out, set_encodek());
		
		lua_pushlstring(L, (char*) out, fsize);
		lua_pushnumber(L, fsize);
		//free(string);

		return 2;
	}

	// nothing to do, return input string 
	if (encoded == 0) {
		//printf(out);
		//printf("size: %d\n", fsize);
		lua_pushlstring(L, string, fsize);
		lua_pushinteger(L, fsize);
		return 2;
	}

	int ret = -1;
	unsigned char *out_data = NULL;
	barr_t *inb = NULL;
	barr_t *outb  = NULL;

	// create buffers
	inb = malloc(sizeof(barr_t));
	if (inb == NULL) {
		lua_pushfstring(L, "Failed to allocate memory");
		lua_pushnil(L);
		return 2;
	}
	inb->length = fsize;
	inb->allocated = fsize;
	inb->buffer = (unsigned char*) string;

	outb = malloc(sizeof(barr_t));
	if (outb == NULL) {
		lua_pushfstring(L, "Failed to allocate memory");
		lua_pushnil(L);
		// cleanup
		free(inb);
		free(inb->buffer);
		free(outb);
		return 2;
	}
	outb->length = 0;
	outb->allocated = 0;
	outb->buffer = out_data;

	ret = decrypt_str(inb, outb);

	if (ret != 0) { // we have an error
		lua_pushfstring(L, "Failed to decrypt config with error %d", ret);
		lua_pushnil(L);
		// cleanup
		free(inb);
		free(inb->buffer);
		free(outb);
		return 2;
	}

	// success, return data
	//printf("%s\n", (char*) outb->buffer);
	lua_pushlstring(L, (char*) outb->buffer, outb->length);
	lua_pushinteger(L, outb->length);

	// cleanup
	free(inb);
	free(inb->buffer);
	free(outb);

	return 2;
}

/**
 * read ini string, unencode if nessecary
 *
 * String contents, int length = read_str2(String file_name)
 * 
 * if length is nil, then contents contains an error message.
 */
static int lua_read_str2(lua_State* L) {
	char* string = (char*) luaL_checkstring(L, 1);
	long fsize = luaL_checkinteger(L, 2);
	
	int encoded = has_magic(string, fsize);

	// old file encryption? down grade.
	if (encoded == 1) {
		char *out;
		out = (char*) malloc(fsize + 1);
		transcode_str(string, &fsize, &out, set_encodek());
		
		lua_pushlstring(L, (char*) out, fsize);
		lua_pushnumber(L, fsize);
		//free(string);
		
		return 2;
	}

	// nothing to do, return input string 
	if (encoded == 0) {
		//printf(out);
		//printf("size: %d\n", fsize);
		lua_pushlstring(L, (char*) string, fsize);
		lua_pushinteger(L, fsize);
		return 2;
	}

	int ret = -1;
	unsigned char *out_data = NULL;
	barr_t *inb = NULL;
	barr_t *outb  = NULL;

	// create buffers
	inb = malloc(sizeof(barr_t));
	if (inb == NULL) {
		lua_pushfstring(L, "Failed to allocate memory");
		lua_pushnil(L);
		return 2;
	}
	inb->length = fsize;
	inb->allocated = fsize;
	inb->buffer = (unsigned char*) string;

	outb = malloc(sizeof(barr_t));
	if (outb == NULL) {
		lua_pushfstring(L, "Failed to allocate memory");
		lua_pushnil(L);
		// cleanup
		free(inb);
		free(outb);
		return 2;
	}
	outb->length = 0;
	outb->allocated = 0;
	outb->buffer = out_data;

	ret = decrypt_str(inb, outb);

	if (ret != 0) { // we have an error
		lua_pushfstring(L, "Failed to decrypt config with error %d", ret);
		lua_pushnil(L);
		// cleanup
		free(inb);
		free(outb);
		return 2;
	}

	// success, return data
	lua_pushlstring(L, (char*) outb->buffer, outb->length);
	lua_pushinteger(L, outb->length);

	//printf("ret: %d\n", ret);
	//return 2;
	
	// cleanup
	free(inb);
	free(outb);

	return 2;
}

//library to be registered
static const struct luaL_Reg libluaph1 [] = {
	{"realpath", os_realpath},
	{"setenv", os_setenv},
	{"getenv", os_getenv},

	// v1, old file encoding mecahnism (depricated)
	{"encode_file", lua_encode_file},
	{"read_ini", lua_read_ini},
	{"read_str", lua_read_str},

	// v2, new file encoding methods
	{"encode_file2", lua_encode_file2},
	{"read_ini2", lua_read_ini2},
	{"read_str2", lua_read_str2},

	{NULL, NULL}  /* sentinel */
};

//name of this function is not flexible
int luaopen_libluaph1 (lua_State *L){
    luaL_newlib(L, libluaph1);
    return 1;
}

#endif

#ifdef __cplusplus
}
#endif
