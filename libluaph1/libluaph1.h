#ifndef LIBLUAPH1_H
#define LIBLUAPH1_H

/* Make this header file easier to include in C++ code */
#ifdef __cplusplus
extern "C" {
#endif

/*
// lua < 5.2 compatibility code
// https://github.com/keplerproject/lua-compat-5.2/blob/master/c-api/compat-5.2.h 

// Line: 155
void luaL_setfuncs (lua_State *L, const luaL_Reg *l, int nup);

// Line: 140
#define luaL_newlib(L, l) \
  (lua_newtable((L)),luaL_setfuncs((L), (l), 0))
*/


#ifdef _WIN32
#define uint uInt
#ifndef WIN32_LEAN_AND_MEAN
#	define WIN32_LEAN_AND_MEAN
#endif

#	define NOGDI 1
#endif

#include <limits.h>
#include <time.h>
#include <stddef.h>
#include "zlib.h"
#include "max_path.h"
#include "osdetection.h"

#define MAGIC {0x30, 0x78, 0x36, 0x36}
#define MAGIC2 {0x30, 0x78, 0x36, 0x37}
#define MAGIC_OFFSET 4

const char *PH_OS = OPERATING_SYSTEM;

typedef struct barr_t {
  unsigned long length;
  unsigned long allocated;
  unsigned char *buffer;
} barr_t;

typedef enum {DECODE, ENCODE} xormode_t;

int decrypt_str(barr_t *in, barr_t *out);
int crypt_str(barr_t *in, barr_t *out);
int has_magic(unsigned char *buffer, long buflen);
unsigned char* xor(unsigned char *string, const char *key, long l);

// private api ?
unsigned char *garbage_data(int len, const char key[], int key_length);
int crypt_file(char *file_in, char* file_out);
int is_encoded(char* string);
char* _xor(char *string, const char *key, long l);
// int transcode_file(char* fin, char* fout, char* key);
int transcode_str(char* strin, long* fsize, char** strout, char* key);

#ifndef PH1_STANDALONE
int luaopen_libluaph1 (lua_State *L);
#endif

#ifdef __cplusplus
}
#endif

#endif
